const express = require("express");
const path = require("path");
const friendsRouter = require("./routes/friends.router.js");
const messageRouter = require("./routes/messages.router.js");

const app = express();
//templating engine to make html pages dynamic
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "views"));

const PORT = 3000;

//Middleware logic
app.use((req, resp, next) => {
	const start = Date.now();
	console.log(`${req.method}  ${req.url}`);
	//using next we jump to next middleware
	next();
	//while returning all this is executed
	const delta = Date.now() - start;
	console.log(delta);
});

app.use("/", express.static(path.join(__dirname, "public")));

app.use(express.json());

//add router to middleware
app.get("/", (req, res) => {
	res.render("index.hbs", {
		title: "Friends Website",
		caption: "Hello EB-15 batch by jenkins",
	});
});

app.use("/friends", friendsRouter);

app.use("/messages", messageRouter);

app.listen(PORT, () => console.log("listening on port 3000"));

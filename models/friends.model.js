const friends = [
	{
		id: 0,
		name: "Chirag",
	},
	{
		id: 1,
		name: "Vidhi",
	},
	{
		id: 2,
		name: "Vamsi",
	},
	{
		id: 3,
		name: "Sai",
	},
	{
		id: 4,
		name: "Sairam",
	},
];

module.exports = friends;

const path = require("path");

function getMessage(req, res) {
	// res.send(
	// 	`<html>
	// 		<body>
	// 			<h1>Messages</h1>
	// 			<ul>
	// 				<li>Message 1</li>
	// 				<li>Message 2</li>
	// 				<li>Message 3</li>
	// 			</ul>
	// 		</body>
	// 	</html>`
	// );

	//when we want to send files like image video etc
	// res.sendFile(
	// 	path.join(__dirname, "..", "public", "images", "skimountain.jpg")
	// );

	//message template
	res.render("messages", {
		title: "Friends",
		friend1: "Rahul",
		friend2: "Rohit",
	});
}

function postMessage(req, res) {
	console.log("updating");
}

module.exports = {
	getMessage,
	postMessage,
};

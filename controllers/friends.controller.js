const model = require("../models/friends.model.js");

function getFriend(req, res) {
	const FriendId = Number(req.params.friendId);
	const friend = model[FriendId];
	if (friend) {
		//directly send response
		res.json(friend);
		//or chain it with status code
		// res.status(200).json(friend);
	} else {
		res.status(404).json({
			error: "Friend does not exist",
		});
	}
}

function getAllFriends(req, res) {
	res.json(model);
}

function postFriend(req, res) {
	//validate the data
	if (!req.body.name) {
		return res.status(400).json({ error: "name is not valid" });
	}
	//  else {
	const newFriend = {
		//to set the req.body as object type we have used another middleware provided by express i.e express.json
		name: req.body.name,
		id: model.length,
	};

	model.push(newFriend);

	res.json(newFriend);
	// }
}

module.exports = {
	getAllFriends,
	getFriend,
	postFriend,
};

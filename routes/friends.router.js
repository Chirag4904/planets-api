const express = require("express");
const friendsRouter = express.Router();
const friendsController = require("../controllers/friends.controller.js");

friendsRouter.use((req, res, next) => {
	console.log(req.ip);
	next();
});

friendsRouter.post("/", friendsController.postFriend);
friendsRouter.get("/", friendsController.getAllFriends);
friendsRouter.get("/:friendId", friendsController.getFriend);

module.exports = friendsRouter;

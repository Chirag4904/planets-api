const express = require("express");

const app = express();

const PORT = 3000;

const friends = [
	{
		id: 0,
		name: "Chirag",
	},
	{
		id: 1,
		name: "Vidhi",
	},
	{
		id: 2,
		name: "Vamsi",
	},
];

//Middleware logic
app.use((req, resp, next) => {
	const start = Date.now();
	console.log(`${req.method}  ${req.url}`);
	//using next we jump to next middleware
	next();
	//while returning all this is executed
	const delta = Date.now() - start;
	console.log(delta);
});

app.use(express.json());

//POST in express
app.post("/friends", (req, res) => {
	//validate the data
	if (!req.body.name) {
		return res.status(400).json({ error: "name is not valid" });
	}
	//  else {
	const newFriend = {
		//to set the req.body as object type we have used another middleware provided by express i.e express.json
		name: req.body.name,
		id: friends.length,
	};

	friends.push(newFriend);

	res.json(newFriend);
	// }
});

app.get("/friends", (req, res) => {
	res.json(friends);
});

app.get("/friends/:friendId", (req, res) => {
	const FriendId = Number(req.params.friendId);
	const friend = friends[FriendId];
	if (friend) {
		//directly send response
		res.json(friend);
		//or chain it with status code
		// res.status(200).json(friend);
	} else {
		res.status(404).json({
			error: "Friend does not exist",
		});
	}
});

app.get("/messages", (req, res) => {
	res.send(
		`<html>
			<body>
				<h1>Messages</h1>
				<ul>
					<li>Message 1</li>
					<li>Message 2</li>
					<li>Message 3</li>
				</ul> 
			</body>
		</html>`
	);
});

app.post("/messages", (req, res) => {
	console.log("updating");
});
app.listen(PORT, () => console.log("listening on port 3000"));
